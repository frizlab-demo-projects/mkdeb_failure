FROM debian:stretch-slim
LABEL maintainer="François Lamboley <francois.lamboley@happn.com>"

ARG ARCH=amd64

RUN apt-get update && apt-get install -y --no-install-recommends \
  git \
  ca-certificates \
  curl \
&& rm -rf /var/lib/apt/lists/*

# Install Go (to install mkdeb…)
WORKDIR "/usr/local"
RUN ["/bin/bash", "-c", "set -eo pipefail && \
  curl -sSL https://dl.google.com/go/go1.12.linux-${ARCH}.tar.gz | tar xz && \
  ln -s /usr/local/go/bin/go /usr/local/bin/go && \
  ln -s /usr/local/go/bin/gofmt /usr/local/bin/gofmt"]
ENV GOROOT=/usr/local/go GOPATH=/usr/local/gohome
# Install mkdeb
RUN set -e && \
    go get mkdeb.sh/cmd/mkdeb && \
    ln -s /usr/local/gohome/bin/mkdeb /usr/local/bin/mkdeb

WORKDIR "/tmp/swift-deb"
COPY swift.tar.bz2 swift.tar.bz2
COPY mkdeb_swiftlang_recipe.yaml swiftlang/recipe.yaml
RUN mkdeb build --from "swift.tar.bz2" --recipe "./swiftlang" --to "./swift.deb" swiftlang:${ARCH}=0.1
